# generate Go code
cd pkg/api
sh generate.sh
# generate JavaScript/TypeScript code
cd ../ui
yarn protoc
cd ../..

go generate -v ./...

go build -a -o zupfmanager-linux
env GOOS=darwin GOARCH=amd64 go build -a -o zupfmanager-osx
