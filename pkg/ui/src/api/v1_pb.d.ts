// package: zpfmgmr
// file: v1.proto

import * as jspb from "google-protobuf";

export class ProjectListRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectListRequest): ProjectListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectListRequest;
  static deserializeBinaryFromReader(message: ProjectListRequest, reader: jspb.BinaryReader): ProjectListRequest;
}

export namespace ProjectListRequest {
  export type AsObject = {
  }
}

export class ProjectListResponse extends jspb.Message {
  clearShortNameList(): void;
  getShortNameList(): Array<string>;
  setShortNameList(value: Array<string>): void;
  addShortName(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectListResponse): ProjectListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectListResponse;
  static deserializeBinaryFromReader(message: ProjectListResponse, reader: jspb.BinaryReader): ProjectListResponse;
}

export namespace ProjectListResponse {
  export type AsObject = {
    shortNameList: Array<string>,
  }
}

export class ProjectCreateRequest extends jspb.Message {
  getShortName(): string;
  setShortName(value: string): void;

  getTitle(): string;
  setTitle(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectCreateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectCreateRequest): ProjectCreateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectCreateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectCreateRequest;
  static deserializeBinaryFromReader(message: ProjectCreateRequest, reader: jspb.BinaryReader): ProjectCreateRequest;
}

export namespace ProjectCreateRequest {
  export type AsObject = {
    shortName: string,
    title: string,
  }
}

export class ProjectCreateResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectCreateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectCreateResponse): ProjectCreateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectCreateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectCreateResponse;
  static deserializeBinaryFromReader(message: ProjectCreateResponse, reader: jspb.BinaryReader): ProjectCreateResponse;
}

export namespace ProjectCreateResponse {
  export type AsObject = {
  }
}

export class ProjectGetRequest extends jspb.Message {
  getShortName(): string;
  setShortName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectGetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectGetRequest): ProjectGetRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectGetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectGetRequest;
  static deserializeBinaryFromReader(message: ProjectGetRequest, reader: jspb.BinaryReader): ProjectGetRequest;
}

export namespace ProjectGetRequest {
  export type AsObject = {
    shortName: string,
  }
}

export class ProjectGetResponse extends jspb.Message {
  hasProject(): boolean;
  clearProject(): void;
  getProject(): Project | undefined;
  setProject(value?: Project): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectGetResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectGetResponse): ProjectGetResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectGetResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectGetResponse;
  static deserializeBinaryFromReader(message: ProjectGetResponse, reader: jspb.BinaryReader): ProjectGetResponse;
}

export namespace ProjectGetResponse {
  export type AsObject = {
    project?: Project.AsObject,
  }
}

export class ProjectUpdateRequest extends jspb.Message {
  hasProject(): boolean;
  clearProject(): void;
  getProject(): Project | undefined;
  setProject(value?: Project): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectUpdateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectUpdateRequest): ProjectUpdateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectUpdateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectUpdateRequest;
  static deserializeBinaryFromReader(message: ProjectUpdateRequest, reader: jspb.BinaryReader): ProjectUpdateRequest;
}

export namespace ProjectUpdateRequest {
  export type AsObject = {
    project?: Project.AsObject,
  }
}

export class ProjectUpdateResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectUpdateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectUpdateResponse): ProjectUpdateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectUpdateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectUpdateResponse;
  static deserializeBinaryFromReader(message: ProjectUpdateResponse, reader: jspb.BinaryReader): ProjectUpdateResponse;
}

export namespace ProjectUpdateResponse {
  export type AsObject = {
  }
}

export class Project extends jspb.Message {
  getShortName(): string;
  setShortName(value: string): void;

  getTitle(): string;
  setTitle(value: string): void;

  clearAssetsList(): void;
  getAssetsList(): Array<ProjectAsset>;
  setAssetsList(value: Array<ProjectAsset>): void;
  addAssets(value?: ProjectAsset, index?: number): ProjectAsset;

  getConf(): Uint8Array | string;
  getConf_asU8(): Uint8Array;
  getConf_asB64(): string;
  setConf(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Project.AsObject;
  static toObject(includeInstance: boolean, msg: Project): Project.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Project, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Project;
  static deserializeBinaryFromReader(message: Project, reader: jspb.BinaryReader): Project;
}

export namespace Project {
  export type AsObject = {
    shortName: string,
    title: string,
    assetsList: Array<ProjectAsset.AsObject>,
    conf: Uint8Array | string,
  }
}

export class ProjectAsset extends jspb.Message {
  getFilename(): string;
  setFilename(value: string): void;

  clearExtractsList(): void;
  getExtractsList(): Array<number>;
  setExtractsList(value: Array<number>): void;
  addExtracts(value: number, index?: number): number;

  getTodo(): string;
  setTodo(value: string): void;

  getProposedBy(): string;
  setProposedBy(value: string): void;

  getPriority(): number;
  setPriority(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProjectAsset.AsObject;
  static toObject(includeInstance: boolean, msg: ProjectAsset): ProjectAsset.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProjectAsset, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProjectAsset;
  static deserializeBinaryFromReader(message: ProjectAsset, reader: jspb.BinaryReader): ProjectAsset;
}

export namespace ProjectAsset {
  export type AsObject = {
    filename: string,
    extractsList: Array<number>,
    todo: string,
    proposedBy: string,
    priority: number,
  }
}

export class AssetListRequest extends jspb.Message {
  getProjectShortName(): string;
  setProjectShortName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssetListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AssetListRequest): AssetListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AssetListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssetListRequest;
  static deserializeBinaryFromReader(message: AssetListRequest, reader: jspb.BinaryReader): AssetListRequest;
}

export namespace AssetListRequest {
  export type AsObject = {
    projectShortName: string,
  }
}

export class AssetListResponse extends jspb.Message {
  clearFilenameList(): void;
  getFilenameList(): Array<string>;
  setFilenameList(value: Array<string>): void;
  addFilename(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssetListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AssetListResponse): AssetListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AssetListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssetListResponse;
  static deserializeBinaryFromReader(message: AssetListResponse, reader: jspb.BinaryReader): AssetListResponse;
}

export namespace AssetListResponse {
  export type AsObject = {
    filenameList: Array<string>,
  }
}

export class AssetInfoRequest extends jspb.Message {
  getProjectShortName(): string;
  setProjectShortName(value: string): void;

  clearFilenameList(): void;
  getFilenameList(): Array<string>;
  setFilenameList(value: Array<string>): void;
  addFilename(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssetInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AssetInfoRequest): AssetInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AssetInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssetInfoRequest;
  static deserializeBinaryFromReader(message: AssetInfoRequest, reader: jspb.BinaryReader): AssetInfoRequest;
}

export namespace AssetInfoRequest {
  export type AsObject = {
    projectShortName: string,
    filenameList: Array<string>,
  }
}

export class AssetInfoResponse extends jspb.Message {
  getInfoMap(): jspb.Map<string, AssetMetadata>;
  clearInfoMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssetInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AssetInfoResponse): AssetInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AssetInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssetInfoResponse;
  static deserializeBinaryFromReader(message: AssetInfoResponse, reader: jspb.BinaryReader): AssetInfoResponse;
}

export namespace AssetInfoResponse {
  export type AsObject = {
    infoMap: Array<[string, AssetMetadata.AsObject]>,
  }
}

export class AssetMetadata extends jspb.Message {
  getTitle(): string;
  setTitle(value: string): void;

  clearExtractsList(): void;
  getExtractsList(): Array<number>;
  setExtractsList(value: Array<number>): void;
  addExtracts(value: number, index?: number): number;

  getDifficulty(): number;
  setDifficulty(value: number): void;

  getPerformanceProbability(): number;
  setPerformanceProbability(value: number): void;

  getGenre(): GenreMap[keyof GenreMap];
  setGenre(value: GenreMap[keyof GenreMap]): void;

  getAuthors(): string;
  setAuthors(value: string): void;

  getCopyrightMusic(): string;
  setCopyrightMusic(value: string): void;

  getCopyrightText(): string;
  setCopyrightText(value: string): void;

  getSource(): string;
  setSource(value: string): void;

  getReferenceCopy(): string;
  setReferenceCopy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssetMetadata.AsObject;
  static toObject(includeInstance: boolean, msg: AssetMetadata): AssetMetadata.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AssetMetadata, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssetMetadata;
  static deserializeBinaryFromReader(message: AssetMetadata, reader: jspb.BinaryReader): AssetMetadata;
}

export namespace AssetMetadata {
  export type AsObject = {
    title: string,
    extractsList: Array<number>,
    difficulty: number,
    performanceProbability: number,
    genre: GenreMap[keyof GenreMap],
    authors: string,
    copyrightMusic: string,
    copyrightText: string,
    source: string,
    referenceCopy: string,
  }
}

export interface GenreMap {
  UNKNOWN: 0;
  SECULAR: 1;
  CHRISTIAN: 2;
}

export const Genre: GenreMap;

