// package: zpfmgmr
// file: v1.proto

import * as v1_pb from "./v1_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ProjectServiceCreate = {
  readonly methodName: string;
  readonly service: typeof ProjectService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.ProjectCreateRequest;
  readonly responseType: typeof v1_pb.ProjectCreateResponse;
};

type ProjectServiceGet = {
  readonly methodName: string;
  readonly service: typeof ProjectService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.ProjectGetRequest;
  readonly responseType: typeof v1_pb.ProjectGetResponse;
};

type ProjectServiceList = {
  readonly methodName: string;
  readonly service: typeof ProjectService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.ProjectListRequest;
  readonly responseType: typeof v1_pb.ProjectListResponse;
};

type ProjectServiceUpdate = {
  readonly methodName: string;
  readonly service: typeof ProjectService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.ProjectUpdateRequest;
  readonly responseType: typeof v1_pb.ProjectUpdateResponse;
};

export class ProjectService {
  static readonly serviceName: string;
  static readonly Create: ProjectServiceCreate;
  static readonly Get: ProjectServiceGet;
  static readonly List: ProjectServiceList;
  static readonly Update: ProjectServiceUpdate;
}

type AssetServiceList = {
  readonly methodName: string;
  readonly service: typeof AssetService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.AssetListRequest;
  readonly responseType: typeof v1_pb.AssetListResponse;
};

type AssetServiceInfo = {
  readonly methodName: string;
  readonly service: typeof AssetService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_pb.AssetInfoRequest;
  readonly responseType: typeof v1_pb.AssetInfoResponse;
};

export class AssetService {
  static readonly serviceName: string;
  static readonly List: AssetServiceList;
  static readonly Info: AssetServiceInfo;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ProjectServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  create(
    requestMessage: v1_pb.ProjectCreateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectCreateResponse|null) => void
  ): UnaryResponse;
  create(
    requestMessage: v1_pb.ProjectCreateRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectCreateResponse|null) => void
  ): UnaryResponse;
  get(
    requestMessage: v1_pb.ProjectGetRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectGetResponse|null) => void
  ): UnaryResponse;
  get(
    requestMessage: v1_pb.ProjectGetRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectGetResponse|null) => void
  ): UnaryResponse;
  list(
    requestMessage: v1_pb.ProjectListRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectListResponse|null) => void
  ): UnaryResponse;
  list(
    requestMessage: v1_pb.ProjectListRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectListResponse|null) => void
  ): UnaryResponse;
  update(
    requestMessage: v1_pb.ProjectUpdateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectUpdateResponse|null) => void
  ): UnaryResponse;
  update(
    requestMessage: v1_pb.ProjectUpdateRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.ProjectUpdateResponse|null) => void
  ): UnaryResponse;
}

export class AssetServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  list(
    requestMessage: v1_pb.AssetListRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.AssetListResponse|null) => void
  ): UnaryResponse;
  list(
    requestMessage: v1_pb.AssetListRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.AssetListResponse|null) => void
  ): UnaryResponse;
  info(
    requestMessage: v1_pb.AssetInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_pb.AssetInfoResponse|null) => void
  ): UnaryResponse;
  info(
    requestMessage: v1_pb.AssetInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_pb.AssetInfoResponse|null) => void
  ): UnaryResponse;
}

