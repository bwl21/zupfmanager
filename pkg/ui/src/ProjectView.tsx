import { createStyles, Theme, withStyles } from '@material-ui/core';
import * as React from 'react';
import { AssetInfoRequest, AssetMetadata, Project, ProjectAsset, ProjectGetRequest, ProjectUpdateRequest } from './api/v1_pb';
import { APIClient } from './client';
import { ProjectAssetsList, InProjectAsset } from './components/InProjectAssetsList';

const styles = (theme: Theme) => createStyles({})

export interface ProjectViewProps {
    client: APIClient;
    project: string
}

interface ProjectViewState {
    project?: Project;
    inProjectAssets: InProjectAsset[];
}

class ProjectViewImpl extends React.Component<ProjectViewProps, ProjectViewState> {

    constructor(p: ProjectViewProps) {
        super(p)
        this.state = {
            inProjectAssets: [],
        };
    }

    componentDidMount() {
        const req = new ProjectGetRequest();
        req.setShortName(this.props.project);
        this.props.client.projects.get(req, (err, resp) => {
            if (!!err) {
                alert(err);
                return;
            }
            this.setState({project: resp?.getProject()});

            const assets = resp?.getProject()?.getAssetsList() || [];
            const assetReq = new AssetInfoRequest();
            assetReq.setProjectShortName(this.props.project);
            assetReq.setFilenameList(assets.map(a => a.getFilename()));
            this.props.client.assets.info(assetReq, (err, resp) => {
                if (!!err) {
                    alert(err);
                    return;
                }

                const res: InProjectAsset[] = (resp?.toObject().infoMap || []).map(e => {return {
                    inProject: assets.find(a => a.getFilename() === e[0])?.toObject(),
                    metadata: e[1]
                }});
                this.setState({ inProjectAssets: res });
            });
        });

        
    }

    render() {
        if (!!this.state.project) {
            return <React.Fragment>
                <div>{this.state.project!.getTitle()}</div>
                <ProjectAssetsList assets={this.state.inProjectAssets} />
                <button onClick={this.addAsset.bind(this)}>Add</button>
            </React.Fragment>;
        }

        return <div>loading ...</div>;
    }

    addAsset() {
        if (!this.state.project) {
            return;
        }

        const nme = prompt("name");
        if (!nme) {
            return;
        }

        const req = new AssetInfoRequest();
        req.setFilenameList([nme]);
        req.setProjectShortName(this.state.project.getShortName());
        this.props.client.assets.info(req, (err, resp) => {
            if (!!err) {
                alert(err);
                return;
            }

            const asset = new ProjectAsset();
            asset.setFilename(nme);
            const prj = this.state.project!;
            prj.addAssets(asset);

            const updateReq = new ProjectUpdateRequest();
            updateReq.setProject(prj);
            this.props.client.projects.update(updateReq, (err, resp) => {
                if (!!err) {
                    alert(err);
                    return;
                }
                window.location.reload();
            });
            
            // , (err) => {
            //     if (!err) {
            //         return;
            //     }
            //     alert(err);
            // });
        });

    }

}

export const ProjectView = withStyles(styles)(ProjectViewImpl);