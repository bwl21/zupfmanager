import { createStyles, Theme } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import * as React from 'react';
import { AssetMetadata, ProjectAsset } from '../api/v1_pb';


const styles = (theme: Theme) => createStyles({})


export interface InProjectAsset {
    metadata?: AssetMetadata.AsObject;
    inProject?: ProjectAsset.AsObject;
}


export interface ProjectAssetListProps {
    assets: InProjectAsset[];
}

const ProjectAssetsListImpl = (props: ProjectAssetListProps) => <ul>
    { props.assets.map((a, i) => <li key={i}>
        <b>Filename:</b> {a.inProject?.filename}<br />
        <b>Title:</b> {a.metadata?.title}<br />
    </li>) }
</ul>;

export const ProjectAssetsList = withStyles(styles)(ProjectAssetsListImpl);