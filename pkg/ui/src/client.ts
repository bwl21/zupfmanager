import { AssetServiceClient, ProjectServiceClient } from "./api/v1_pb_service";

export interface APIClient {
    projects: ProjectServiceClient;
    assets: AssetServiceClient;
}