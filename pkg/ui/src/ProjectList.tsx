import { createStyles, Theme, withStyles } from '@material-ui/core';
import * as React from 'react';
import { ProjectListRequest } from './api/v1_pb';
import { ProjectServiceClient } from "./api/v1_pb_service";
import { APIClient } from './client';

const styles = (theme: Theme) => createStyles({})

interface ProjectListProps {
    client: APIClient
}

interface ProjectListState {
    projects: string[]
}

class ProjectListImpl extends React.Component<ProjectListProps, ProjectListState> {

    constructor(p: ProjectListProps) {
        super(p)
        this.state = {projects: []};
    }

    componentDidMount() {
        this.props.client.projects.list(new ProjectListRequest(), (err, resp) => {
            if (!!err) {
                alert(err);
                return;
            }

            this.setState({projects: resp?.getShortNameList() || []});
        })
    }

    render() {
        return <ul>
            { this.state.projects.map((v, i) => <li key={i}><a href={"/project/"+v}>{v}</a></li>) }
        </ul>
    }

}

export const ProjectList = withStyles(styles)(ProjectListImpl);