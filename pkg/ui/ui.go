package ui

import (
	"embed"
	"io/fs"

	"net/http"
)

//go:generate yarn build
//go:embed build
var embededFiles embed.FS

var FS http.FileSystem = func() http.FileSystem {
	sub, err := fs.Sub(embededFiles, "build")
	if err != nil {
		panic(err)
	}
	return http.FS(sub)
}()
