package projects

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/bwl21/zupfmanager/pkg/assets"
)

type ProjectStore struct {
	Path          string
	AssetBasePath string

	index map[string]*Project
}

const (
	projectConfigFN = "project-config.json"
)

func NewProjectStore(path, assetBasePath string) *ProjectStore {
	return &ProjectStore{
		Path:          path,
		AssetBasePath: assetBasePath,
		index:         make(map[string]*Project),
	}
}

func (s *ProjectStore) Create(shortName, title string, assetSearchPaths []string) error {
	path := filepath.Join(s.Path, shortName)
	err := os.MkdirAll(path, 0755)
	if err != nil {
		return err
	}

	return s.update(&Project{
		ShortName: shortName,
		Metadata: &ProjectMetadata{
			Title: title,
		},
		SearchPaths: assetSearchPaths,
	})
}

// update writes the project metadata to disk
func (s *ProjectStore) update(p *Project) error {
	fc, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		return err
	}
	fn := filepath.Join(s.Path, p.ShortName, projectConfigFN)
	return ioutil.WriteFile(fn, fc, 0644)
}

func (s *ProjectStore) Close() error {
	return nil
}

func (s *ProjectStore) List() ([]string, error) {
	fis, err := ioutil.ReadDir(s.Path)
	if err != nil {
		return nil, err
	}

	var res []string
	for _, fi := range fis {
		if !fi.IsDir() {
			continue
		}

		res = append(res, fi.Name())
	}
	return res, nil
}

var ErrNotAProject = errors.New("shortname does not point to a directory")

func (s *ProjectStore) Get(shortname string) (*Project, error) {
	path := filepath.Join(s.Path, shortname)
	if stat, err := os.Stat(path); stat != nil && !stat.IsDir() {
		return nil, ErrNotAProject
	} else if os.IsNotExist(err) {
		return nil, os.ErrNotExist
	} else if err != nil {
		return nil, err
	}

	fc, err := ioutil.ReadFile(filepath.Join(path, projectConfigFN))
	if err != nil {
		return nil, err
	}
	var p Project
	p.s = s
	err = json.Unmarshal(fc, &p)
	if err != nil {
		return nil, err
	}
	p.ShortName = shortname

	p.AllAssets, err = assets.NewFileStorage(s.AssetBasePath, p.SearchPaths...)
	if err != nil {
		return nil, err
	}
	

	return &p, nil
}

type Project struct {
	s *ProjectStore

	ShortName string          `json:"-"`
	AllAssets *assets.Storage `json:"-"`

	Metadata    *ProjectMetadata `json:"project"`
	Assets      []*ProjectAsset  `json:"assets"`
	SearchPaths []string         `json:"searchPaths"`
}

// Modify modifies the project metadata and saves the result to disk.
// If saving to disk fails, the modifictions will still persist on the in-memory representation.
func (p *Project) Modify(f func(p *Project)) error {
	f(p)
	return p.s.update(p)
}

type ProjectMetadata struct {
	Title  string          `json:"title"`
	Config json.RawMessage `json:"$conf"`
}

type ProjectAsset struct {
	Filename string  `json:"filename"`
	Extracts []int32 `json:"extracts,omitempty"`
}
