package assets

import (
	"io/fs"
	"testing"
	"testing/fstest"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/bwl21/zupfmanager/pkg/api"
)

func TestLoadAssetMetadata(t *testing.T) {
	type Expectation struct {
		Asset *api.AssetMetadata
		Error string
	}
	tests := []struct {
		Name        string
		FS          fs.FS
		Path        string
		Expectation Expectation
	}{
		{
			Name: "not found",
			FS:   fstest.MapFS{},
			Path: "hello.abc",
			Expectation: Expectation{
				Error: "open hello.abc: file does not exist",
			},
		},
		{
			Name: "load without config",
			FS: fstest.MapFS{
				"hello.abc": &fstest.MapFile{
					Data: []byte("T:hello world\n"),
				},
			},
			Path: "hello.abc",
			Expectation: Expectation{
				Asset: &api.AssetMetadata{
					Title: "hello world",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			var (
				act Expectation
				err error
			)
			act.Asset, err = loadAssetMetadata(test.FS, test.Path)
			if err != nil {
				act.Error = err.Error()
			}

			if diff := cmp.Diff(test.Expectation, act, cmpopts.IgnoreUnexported(api.AssetMetadata{})); diff != "" {
				t.Errorf("loadAssetMetadata() mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
