package assets

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bwl21/zupfmanager/pkg/api"
)

func NewFileStorage(root string, paths ...string) (*Storage, error) {
	rootfs := os.DirFS("/")
	searchPaths := make([]string, len(paths))
	for i, pth := range paths {
		if filepath.IsAbs(pth) {
			searchPaths[i] = pth
		} else {
			searchPaths[i] = filepath.Join(root, pth)
		}
		searchPaths[i] = strings.TrimPrefix(searchPaths[i], "/")
	}

	res := &Storage{
		FS:          rootfs,
		SearchPaths: searchPaths,
		index:       make(map[string]*Asset),
	}
	err := res.Refresh()
	if err != nil {
		return nil, err
	}
	return res, nil
}

type Storage struct {
	FS          fs.FS
	SearchPaths []string

	index map[string]*Asset
}

func (s *Storage) Refresh() error {
	idx := make(map[string]*Asset)
	for _, p := range s.SearchPaths {
		fns, err := fs.Glob(s.FS, filepath.Join(p, "*.abc"))
		if err != nil {
			log.WithError(err).WithField("searchPath", p).Warn("cannot scan asset search path")
			continue
		}
		for _, fn := range fns {
			if _, exists := idx[fn]; exists {
				return nil
			}

			md, err := loadAssetMetadata(s.FS, fn)
			if err != nil {
				log.WithField("fn", fn).WithError(err).Warn("cannot load metadata of this asset")
				return nil
			}

			idx[filepath.Base(fn)] = &Asset{
				Path:     fn,
				Metadata: md,
			}
		}
		if err != nil {
			return err
		}
	}
	s.index = idx
	return nil
}

func (s *Storage) List() []*Asset {
	res := make([]*Asset, 0, len(s.index))
	for _, a := range s.index {
		res = append(res, a)
	}
	return res
}

func (s *Storage) Get(filename string) *Asset {
	return s.index[filename]
}

func loadAssetMetadata(fs fs.FS, path string) (*api.AssetMetadata, error) {
	f, err := fs.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var (
		res         api.AssetMetadata
		jsonfc      string
		inJsonBlock bool
	)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "T:") {
			res.Title = strings.TrimSpace(strings.TrimPrefix(line, "T:"))
			continue
		}
		if strings.HasPrefix(line, "C:") {
			res.Authors = strings.TrimSpace(strings.TrimPrefix(line, "C:"))
			continue
		}

		if strings.HasPrefix(line, "%%%%zupfnoter") {
			if inJsonBlock {
				inJsonBlock = false
				continue
			}

			if strings.HasPrefix(line, "%%%%zupfnoter.config") {
				inJsonBlock = true
				continue
			}
		}
		if inJsonBlock {
			jsonfc += line
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	if len(jsonfc) == 0 {
		return &res, nil
	}

	var cfg struct {
		Product []int `json:"produce"`
		Extract map[string]struct {
			Notes struct {
				CopyrightMusic struct {
					Text string `json:"text"`
				} `json:"T02_copyright_music"`
			} `json:"notes"`
		}
	}
	err = json.Unmarshal([]byte(jsonfc), &cfg)
	if err != nil {
		return nil, fmt.Errorf("cannot parse asset %s metadata: %w", path, err)
	}

	extracts := make([]int32, len(cfg.Product))
	for i, v := range cfg.Product {
		extracts[i] = int32(v)
	}
	res.Extracts = extracts

	e0, ok := cfg.Extract["0"]
	if !ok {
		return nil, fmt.Errorf("asset %s has no extract 0", path)
	}
	res.CopyrightMusic = e0.Notes.CopyrightMusic.Text

	return &res, nil
}

type Asset struct {
	Path     string
	Metadata *api.AssetMetadata
}

func (a Asset) Filename() string {
	return filepath.Base(a.Path)
}
