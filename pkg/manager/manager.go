package manager

import (
	"context"
	"errors"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/bwl21/zupfmanager/pkg/api"
	"gitlab.com/bwl21/zupfmanager/pkg/projects"
	"gitlab.com/bwl21/zupfmanager/pkg/ui"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

// New creates a new Zupfmanager
func New(c *Config) (*Manager, error) {
	return &Manager{
		Config:   c,
		Projects: projects.NewProjectStore(c.Projects, c.AssetBasePath),
	}, nil
}

type Manager struct {
	Config   *Config
	Projects *projects.ProjectStore
}

// Start starts the service and blocks until the context is canceled.
func (m *Manager) Start(ctx context.Context) error {
	var webuiServer http.Handler
	if m.Config.DebugProxy != "" {
		tgt, err := url.Parse(m.Config.DebugProxy)
		if err != nil {
			// this is debug only - it's ok to panic
			panic(err)
		}

		log.WithField("target", tgt).Debug("proxying to webui server")
		webuiServer = httputil.NewSingleHostReverseProxy(tgt)
	} else {
		// WebUI is a single-page app, hence any path that does not resolve to a static file must result in /index.html.
		// As a (rather crude) fix we intercept the response writer to find out if the FileServer returned an error. If so
		// we return /index.html instead.
		dws := http.FileServer(ui.FS)
		webuiServer = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			dws.ServeHTTP(&interceptResponseWriter{
				ResponseWriter: w,
				errH: func(rw http.ResponseWriter, code int) {
					r.URL.Path = "/"
					rw.Header().Set("Content-Type", "text/html; charset=utf-8")
					dws.ServeHTTP(rw, r)
				},
			}, r)
		})
	}

	// Logrus entry is used, allowing pre-definition of certain fields by the user.
	logrusEntry := logrus.NewEntry(logrus.StandardLogger())
	grpcServer := grpc.NewServer(
		grpc_middleware.WithUnaryServerChain(
			grpc_logrus.UnaryServerInterceptor(logrusEntry),
		),
		grpc_middleware.WithStreamServerChain(
			grpc_logrus.StreamServerInterceptor(logrusEntry),
		),
	)
	api.RegisterAssetServiceServer(grpcServer, &assetService{m: m})
	api.RegisterProjectServiceServer(grpcServer, &projectService{m: m})
	reflection.Register(grpcServer)

	grpcWebServer := grpcweb.WrapServer(grpcServer)

	mux := http.NewServeMux()
	mux.Handle("/", hstsHandler(
		grpcTrafficSplitter(
			webuiServer,
			grpcWebServer,
		),
	))
	defer m.Projects.Close()
	defer grpcServer.GracefulStop()

	log.WithField("addr", m.Config.Address).Info("serving zupfmanager service")
	go func() {
		err := http.ListenAndServe(m.Config.Address, mux)
		if err != nil {
			log.WithField("addr", m.Config.Address).WithError(err).Fatal("cannot serve web service")
		}
	}()

	<-ctx.Done()

	return nil
}

type assetService struct {
	m *Manager

	api.UnimplementedAssetServiceServer
}

func (s *assetService) List(ctx context.Context, req *api.AssetListRequest) (*api.AssetListResponse, error) {
	prj, err := s.m.Projects.Get(req.ProjectShortName)
	if err != nil {
		return nil, convertProjectGetErr(err)
	}

	assets := prj.AllAssets.List()
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}

	res := make([]string, len(assets))
	for i, f := range assets {
		res[i] = f.Filename()
	}

	return &api.AssetListResponse{Filename: res}, nil
}

func (s *assetService) Info(ctx context.Context, req *api.AssetInfoRequest) (*api.AssetInfoResponse, error) {
	prj, err := s.m.Projects.Get(req.ProjectShortName)
	if err != nil {
		return nil, convertProjectGetErr(err)
	}

	res := make(map[string]*api.AssetMetadata, len(req.Filename))
	for _, fn := range req.Filename {
		a := prj.AllAssets.Get(fn)
		if a == nil {
			return nil, status.Errorf(codes.NotFound, "asset %s does not exist", fn)
		}

		res[fn] = a.Metadata
	}

	return &api.AssetInfoResponse{Info: res}, nil
}

type projectService struct {
	m *Manager

	api.UnimplementedProjectServiceServer
}

func (s *projectService) Create(ctx context.Context, req *api.ProjectCreateRequest) (*api.ProjectCreateResponse, error) {
	if req.ShortName == "" {
		return nil, status.Errorf(codes.InvalidArgument, "short_name must not be empty")
	}
	if req.Title == "" {
		return nil, status.Error(codes.InvalidArgument, "title must not be empty")
	}

	_, err := s.m.Projects.Get(req.ShortName)
	if err == nil {
		return nil, status.Errorf(codes.AlreadyExists, "project %s already exists", req.ShortName)
	}

	err = s.m.Projects.Create(req.ShortName, req.Title, s.m.Config.Assets)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}

	return &api.ProjectCreateResponse{}, nil
}

func (s *projectService) Get(ctx context.Context, req *api.ProjectGetRequest) (*api.ProjectGetResponse, error) {
	prj, err := s.m.Projects.Get(req.ShortName)
	if err != nil {
		return nil, convertProjectGetErr(err)
	}

	assets := make([]*api.ProjectAsset, len(prj.Assets))
	for i, a := range prj.Assets {
		assets[i] = &api.ProjectAsset{
			Filename: a.Filename,
			Extracts: a.Extracts,
		}
	}

	return &api.ProjectGetResponse{
		Project: &api.Project{
			ShortName: prj.ShortName,
			Title:     prj.Metadata.Title,
			Conf:      prj.Metadata.Config,
			Assets:    assets,
		},
	}, nil
}

func (s *projectService) List(context.Context, *api.ProjectListRequest) (*api.ProjectListResponse, error) {
	prjs, err := s.m.Projects.List()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	return &api.ProjectListResponse{ShortName: prjs}, nil
}

func (s *projectService) Update(ctx context.Context, req *api.ProjectUpdateRequest) (*api.ProjectUpdateResponse, error) {
	prj, err := s.m.Projects.Get(req.Project.ShortName)
	if err != nil {
		return nil, convertProjectGetErr(err)
	}

	err = prj.Modify(func(p *projects.Project) {
		assets := make([]*projects.ProjectAsset, len(p.Assets))
		for i, a := range p.Assets {
			assets[i] = &projects.ProjectAsset{
				Filename: a.Filename,
				Extracts: a.Extracts,
			}
		}
		for _, a := range req.Project.Assets {
			assets = append(assets, &projects.ProjectAsset{
				Filename: a.Filename,
				Extracts: a.Extracts,
			})
		}

		p.Metadata.Config = req.Project.Conf
		p.Metadata.Title = req.Project.Title
		p.Assets = assets
	})
	if err != nil {
		return nil, convertProjectGetErr(err)
	}

	return &api.ProjectUpdateResponse{}, nil
}

func convertProjectGetErr(err error) error {
	if errors.Is(err, os.ErrNotExist) {
		return status.Error(codes.NotFound, "project does not exist")
	} else if errors.Is(err, projects.ErrNotAProject) {
		return status.Error(codes.InvalidArgument, err.Error())
	} else if err != nil {
		return status.Error(codes.Unknown, err.Error())
	}

	return nil
}
