package manager

type Config struct {
	// DebugProxy helps debug the UI
	DebugProxy string `json:"debugProxy,omitempty"`

	// AssetBasePath is the path all asset search paths are relative to
	AssetBasePath string `json:"-"`

	// Assets is a list of asset search paths
	Assets []string `json:"assets"`
	// Projects is the path pointing the root of all projects
	Projects string `json:"projects"`

	// Address is the web server address of the service offered here
	Address string `json:"addr"`
}
