#!/bin/bash

install_dependencies() {
    go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.27.1

    go get google.golang.org/protobuf/runtime/protoimpl@v1.27.1
    go get google.golang.org/protobuf/reflect/protoreflect@v1.27.1
	go get google.golang.org/protobuf/types/known/timestamppb@v1.27.1

    go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1.0

    go install github.com/golang/mock/mockgen@v1.6.0

    go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@v2.5.0
    go install github.com/vadimi/grpc-client-cli/cmd/grpc-client-cli@v1.10.0

    curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.17.3/protoc-3.17.3-linux-x86_64.zip
    unzip protoc-3.17.3-linux-x86_64.zip -d /workspace/protoc
    rm protoc-3.17.3-linux-x86_64.zip
}

install_dependencies