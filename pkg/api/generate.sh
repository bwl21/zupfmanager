#!/bin/bash

go_protoc() {
    # shellcheck disable=2035
    /workspace/protoc/bin/protoc \
        -I/tmp/protoc/include -I. \
        --go_out=. \
        --go_opt=paths=source_relative \
        --go-grpc_out=. \
        --go-grpc_opt=paths=source_relative \
        *.proto
}

go_protoc