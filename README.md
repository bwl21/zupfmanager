# zupfmanager

An complement of Zupfnoter to manage Zupfnoter files and Tune-booklets

## How to develop

### How to run the server
```bash
go run main.go run --config config.json --debug-ui
```

### Build a binary
```bash
go generate -v ./...
go build
```

### How to debug
- use "debug test" in unit tests (preferred)
- use "Debug zupfmanager server" in the _Run and Debug_ view

### How to re-generate the API
1. make sure the dependencies are installed (they are always in a Gitpod workspace), e.g. by running `pkg/api/deps.sh`
2. Generate stuff
    ```bash
    # generate Go code
    cd pkg/api
    sh generate.sh
    # generate JavaScript/TypeScript code
    cd ../ui
    yarn protoc
    ```


# Questions

1. why do we generate the probuf stuff for js within Yarn. Could we do it in generate.sh as well?
2. how can we distinguish generated code from manually edited code
3. I could not see the control flow, e.g.  where are the propbuf requests made?

* handle Warnings

  status.go only provides error

  if an asset does not exist, I would like to raise a waring such that the entry in the project file does not go away.

  usecae: for planning we could plan with non existing assets.

  
