package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/bwl21/zupfmanager/pkg/assets"
	"gitlab.com/bwl21/zupfmanager/pkg/manager"
)

func main() {
	app := &cli.App{
		Name:  "zupfmanager",
		Usage: "manage zupfnoter files",
		Commands: []*cli.Command{
			{
				Name:  "run",
				Usage: "runs zupfmanager",
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:     "config",
						Usage:    "path to the config file",
						Required: true,
					},
					&cli.BoolFlag{
						Name:  "debug-ui",
						Usage: "enables hot-reloading of the UI",
					},
				},
				Action: func(c *cli.Context) error {
					return run(c.Path("config"), c.Bool("debug-ui"))
				},
			},
			{
				Name:  "example-config",
				Usage: "produces an example config file",
				Action: func(c *cli.Context) error {
					enc := json.NewEncoder(os.Stdout)
					enc.SetIndent("", "  ")
					return enc.Encode(manager.Config{
						Projects: "projects/",
						Assets: []string{
							"assets/",
							"other-assets/",
						},
						Address: "0.0.0.0:8080",
					})
				},
			},
			{
				Name: "debug",
				Subcommands: []*cli.Command{
					{
						Name: "dump-assets",
						Action: func(c *cli.Context) error {
							wd, err := os.Getwd()
							if err != nil {
								return err
							}
							s, err := assets.NewFileStorage(wd, "")
							if err != nil {
								return err
							}

							for _, a := range s.List() {
								fmt.Println(a.Path)
							}
							return nil
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func run(cfgfn string, debugUI bool) error {
	log.SetLevel(log.DebugLevel)

	fc, err := ioutil.ReadFile(cfgfn)
	if err != nil {
		return fmt.Errorf("cannot read config: %w", err)
	}

	var cfg manager.Config
	err = json.Unmarshal(fc, &cfg)
	if err != nil {
		return fmt.Errorf("cannot unmarshal config: %w", err)
	}
	cfg.AssetBasePath = filepath.Dir(cfgfn)

	if debugUI {
		cfg.DebugProxy = "http://localhost:3000"

		wd, err := os.Getwd()
		if err != nil {
			logrus.WithError(err).Fatal("cannot get current working directory")
		}

		debugsrvLogs, err := os.OpenFile("ui-debug.log", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
		if err != nil {
			logrus.WithError(err).Fatal("cannot open debug log for writing")
		}
		defer debugsrvLogs.Close()

		cmd := exec.Command("yarn", "start")
		cmd.Dir = filepath.Join(wd, "pkg", "ui")
		cmd.Stderr = debugsrvLogs
		cmd.Stdout = debugsrvLogs

		err = cmd.Start()
		if err != nil {
			log.WithError(err).Fatal("cannot start debug server")
		}
		defer cmd.Process.Signal(syscall.SIGTERM)

		// give yarn some time to boot
		time.Sleep(10 * time.Second)
	}

	mgmt, err := manager.New(&cfg)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithCancel(context.Background())
	ec := make(chan error, 1)
	go func() {
		defer close(ec)

		log.Infof("server running on %s", cfg.Address)
		ec <- mgmt.Start(ctx)
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	select {
	case <-c:
		log.Info("shutting down")
		cancel()
		return <-ec
	case err := <-ec:
		cancel()
		return err
	}
}
